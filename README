Welcome to DINA Repository
-------------------------------

1. GNU General Public License

This repo contains free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

--

2. DINA method Description

This repo contains the scripts useful to execute the DINA method.

The full paper Gambardella et al., Differential network analysis 
for the identification of condition-specific pathway activity and 
regulation. (Bioinformatics, 2013), can be found here:

http://bioinformatics.oxfordjournals.org/content/29/14/1776

Summary: We developed a network-based algorithm, DINA, which is able to 
identify sets of genes which are significantly co-regulated only 
in specific conditions, but not in others. The algorithm starts 
with a set of M genes ( i.e. genes belonging to the same pathway ) 
and a set of N networks ( i.e. the thirty tissue-specific gene 
networks ). It then computes a ''co-regulation probability'' for
the M genes in each of the N networks; this probability is 
proportional to the number of edges among the genes in each of
the networks.  DINA then quantifies how variable the co-regulation
probability is across the N networks. Variability is quantified
using an entropy-based measure (H).

--

3. Repository Description

1. The folder mtx/ contains the 30 tissue specific gene-networks (of these matrices
   only the low triangle is stored, since they are symmetric) inferred in 
   Gambardella et al. (Bioinformatics, 2013).
   
2. gene_map.mat --> contains the corresponding gene for each row the adj matrices in 1.

3. dina.m --> DINA method.